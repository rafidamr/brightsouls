#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "enemy.h"

//CREATE ENEMY
//MANGAMBIL SATU BARIS INFORMASI PADA FILE EKSTERNAL
void CREATEENEMY(enemy * x){

	Hp(*x) = Str(*x) = Def(*x) = Exp(*x) = Panjang(*x) = 0;
	CreateEmptyStack(&(*x).act);
	
	IgnoreBlank();
	NAMA(x);
	HP(x);
	STR(x);
	DEF(x);
	EXP(x);
	ACT(x);

}

//NAMA ENEMY
void NAMA(enemy * x){
	int i = 1;
	while(valid){
		Huruf(i,*x) = CC;
		ADV();
		Panjang(*x)+=1;
		i++;
	}
	IgnoreBlank();
}

//HP ENEMY
void HP(enemy * x){
	int base = 1;	
	while(valid){
		Hp(*x) = Hp(*x)*base + (CC - '0');
		base = 10;
		ADV();
	}
	IgnoreBlank();
}

//STR ENEMY
void STR(enemy * x){
	int base = 1;	
	while(valid){
		Str(*x) = Str(*x)*base + (CC - '0');
		base = 10;
		ADV();
	}
	IgnoreBlank();
}

//DEF ENEMY
void DEF(enemy * x){
	int base = 1;	
	while(valid){
		Def(*x) = Def(*x)*base + (CC - '0');
		base = 10;
		ADV();
	}
	IgnoreBlank();
}

//EXP ENEMY
void EXP(enemy * x){
	int base = 1;	
	while(valid){
		Exp(*x) = Exp(*x)*base + (CC - '0');
		base = 10;
		ADV();
	}
	IgnoreBlank();
}

//ACT ENEMY
//40 AKSI ENEMY DIMASUKAN KE STACK (80 AKSI UNTUK BOS)
void ACT(enemy * x){

	srand(time(NULL));

	char r[5];	int i,j,random;
	i = 1;
	
	while(valid){
		r[i] = CC;
		if(i == 4){
			for(j=1;j<=4;++j){
				random = rand() % 4 + 1;
				Push(&Act(*x),r[random]);
			}
			i = 0;
		}
		i++;
		ADV();
	}

	IgnoreBlank();
}

//PRINT ENEMY
//PRINT SEMUA IDENTITAS ENEMY LALU MENRANSFER AKSI ENEMY MENGGUNAKAN PROSEDUR TACT
void PRINTENEMY(enemy x){
	int i;

	printf("NAMA: ");
	for (i = 1; i <= Panjang(x); ++i)
		printf("%c",x.nama.kata[i]);
	printf("\n");

	printf("HP  : %d\n",Hp(x));
	printf("STR : %d\n",Str(x));
	printf("DEF : %d\n",Def(x));
	printf("EXP : %d\n",Exp(x));

	int stack = 10;

	//VALIDASI UNTUK ENEMY BOSS
	if(Huruf(1,x) == 'D' && Huruf(2,x) == 'o' && Huruf(3,x) == 'o' && Huruf(4,x) == 'm')
		stack = 20;

	for(i = 1; i<=stack ; i++)
		TACT(&x);

}

//TRANSFER ACT
//MEMINDAHKAN 4 BUAH AKSI ENEMY (1 SET) KE DALAM QUEUE
void TACT(enemy * x){
	int i;	char aksi;	Queue q;
	CreateEmptyQueue(&q);
	
	for(i=1;i<=4;i++){
		Pop(&Act(*x),&aksi);
		Add(&q,aksi);
	}

	PRINTQUEUE(q);
}

//PRINT QUEUE
//MENCETAK 4 BUAH AKSI ENEMY
void PRINTQUEUE(Queue q){
	infotypequeue turn;
	char c[5]; int i;
	
	for(i = 1;i<=4;++i){
		Del(&q,&turn);
		c[i] = turn;
		if(i%2!=0)
			printf("%c",turn);
		else
			printf("#");
	}
	printf("\n");
}