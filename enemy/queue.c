//NIM				: 13515125
//Nama				: Muhammad Rafid Amrullah
//Tanggal			: 05/10/2016
//Topik praktikum	: queue
//Deskripsi			: primitif queue

/* File : queue.c */
/* Definisi ADT Queue dengan representasi array secara eksplisit dan alokasi statik */
/* Model Implementasi Versi III dengan circular buffer */

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
/* Konstanta untuk mendefinisikan address tak terdefinisi */
/* Definisi Queue kosong: HEAD=Nil; TAIL=Nil. */
/* Catatan implementasi: T[0] tidak pernah dipakai */

/* ********* Prototype ********* */
boolean IsEmptyQueue (Queue Q)
/* Mengirim true jika Q kosong: lihat definisi di atas */
{
	return (Head(Q) == Nil)&&(Tail(Q) == Nil);
}
boolean IsFullQueue (Queue Q)
/* Mengirim true jika tabel penampung elemen Q sudah penuh */
/* yaitu mengandung elemen sebanyak MaxEl */
{	
	return NBElmtQueue(Q) == MaxEl;
}
int NBElmtQueue (Queue Q)
/* Mengirimkan banyaknya elemen queue. Mengirimkan 0 jika Q kosong. */
{
	
	if (IsEmptyQueue(Q))
		return 0;
	else
		return (Tail(Q) - Head(Q) + MaxEl) % MaxEl + 1;
}
/* *** Kreator *** */
void CreateEmptyQueue (Queue * Q)//, int Max)
/* I.S. sembarang */
/* F.S. Sebuah Q kosong terbentuk dan salah satu kondisi sbb: */
/* Jika alokasi berhasil, Tabel memori dialokasi berukuran Max+1 */
/* atau : jika alokasi gagal, Q kosong dg MaxEl=0 */
/* Proses : Melakukan alokasi, membuat sebuah Q kosong */
{	
	Tail(*Q) = Nil;
	Head(*Q) = Nil;

	// DeAlokasiQueue(Q);
	// (*Q).T = (infotypequeue*) malloc((Max + 1) * sizeof(infotypequeue));
	// MaxEl(*Q) = Max;
}
/* *** Destruktor *** */
// void DeAlokasiQueue(Queue * Q)
// /* Proses: Mengembalikan memori Q */
// /* I.S. Q pernah dialokasi */
//  F.S. Q menjadi tidak terdefinisi lagi, MaxEl(Q) diset 0 
// {
// 	if((*Q).T != Nil)
// 		free((*Q).T);
// 	MaxEl(*Q) = 0;
// 	Tail(*Q) = 0;
// 	Head(*Q) = 0;
// }
/* *** Primitif Add/Delete *** */
void Add (Queue * Q, infotypequeue X)
/* Proses: Menambahkan X pada Q dengan aturan FIFO */
/* I.S. Q mungkin kosong, tabel penampung elemen Q TIDAK penuh */
/* F.S. X menjadi TAIL yang baru, TAIL "maju" dengan mekanisme circular buffer */
{
	if(!IsFullQueue(*Q)){
		if (IsEmptyQueue(*Q)){
			Head(*Q) = 1;
		}
		Tail(*Q) = (Tail(*Q) % MaxEl) + 1;
		InfoTail(*Q) = X;
	}
}
void Del (Queue * Q, infotypequeue * X)
/* Proses: Menghapus X pada Q dengan aturan FIFO */
/* I.S. Q tidak mungkin kosong */
/* F.S. X = nilai elemen HEAD pd I.S., HEAD "maju" dengan mekanisme circular buffer; 
        Q mungkin kosong */
{
	if(!IsEmptyQueue(*Q)){
		*X = InfoHead(*Q);
		if (NBElmtQueue(*Q) == 1){
			Head(*Q) = Nil;
			Tail(*Q) = Nil;
		}
		else
			Head(*Q) = Head(*Q) % MaxEl + 1;
	}
}