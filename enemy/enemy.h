#ifndef enemy_H
#define enemy_H

#include "queue.h"
#include "stackt.h"
#include "mesinkar.h"
#include "mesinkata.h"

#define  valid (CC != BLANK && CC != MARK && CC != COMMA)
#define max_nama 100


//TIPE BENTUKAN UNTUK NAMA
typedef struct{
	char kata[max_nama+1];	//maks nama enemy 100 (sepuluh) digit
	int length;
}id;


//TIPE BENTUKAN UNTUK SATU ENEMY
typedef struct{
	id nama;
	int hp;
	int str;
	int def;
	int exp;
	Stack act;
}enemy;


//SELEKTOR
#define Huruf(i,x) (x).nama.kata[i]
#define Panjang(x) (x).nama.length
#define Hp(x) (x).hp
#define Str(x) (x).str
#define Def(x) (x).def
#define Exp(x) (x).exp
#define Act(x) (x).act

//CREATE ENEMY
void CREATEENEMY(enemy * x);

//NAMA ENEMY
void NAMA(enemy * x);

//HP ENEMY
void HP(enemy * x);

//STR ENEMY
void STR(enemy * x);

//DEF ENEMY
void DEF(enemy * x);

//EXP ENEMY
void EXP(enemy * x);

//ACT ENEMY
//40 AKSI ENEMY DIMASUKAN KE STACK
void ACT(enemy * x);

//TRANSFER ACT
//MEMINDAHKAN 4 BUAH AKSI ENEMY (1 SET) KE DALAM QUEUE
void TACT(enemy * x);

//PRINT QUEUE
//MENCETAK 4 BUAH AKSI ENEMY DALAM BATTLE
void PRINTQUEUE(Queue q);	

//PRINT ENEMY
void PRINTENEMY(enemy x);

#endif